package com.example.simplefragmentapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.simplefragmentapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /*
        * Untuk load fragment Anda harus menggunakan
        * Tag XML Fragment Container View atau Fragment
        * For the first time, kalian bisa loadFragment Home.
        * */
        loadFragment(HomeFragment())

        /*
        * Untuk mengganti halaman anda bisa gunakan bottom navigation
        * */
        binding.bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.homeFeed -> {
                    loadFragment(HomeFragment())
                    return@setOnItemSelectedListener true
                }
                /*
                * Jika anda ingin passing data dari Activity ke Fragment
                * anda bisa manfaatkan bundle.
                * Sebagai contoh: Saya menggunakan Home Fragment tapi saya passing data String yang berbeda.
                * */
                R.id.homeShop -> {
                    val text = "Ini Shop Fragment"
                    val bundle = Bundle()
                    bundle.putString(KEY_TEXT, text)
                    loadFragment(HomeFragment(), bundle)
                    return@setOnItemSelectedListener true
                }
                R.id.homeHistory -> {
                    val text = getString(R.string.history_fragment)
                    val bundle = Bundle()
                    bundle.putString(KEY_TEXT, text)
                    loadFragment(HomeFragment(), bundle)
                    return@setOnItemSelectedListener true
                }
                R.id.homeProfile -> {
                    loadFragment(ProfileFragment())
                    return@setOnItemSelectedListener true
                }
            }
            false
        }
    }

    /*
    * Anda bisa membuat fungsi untuk meload sebuah Fragment
    * Gunakan supportFragmentManager di activity
    * Anda bisa menggunakan berbagai fungsi fragment transaction
    * seperti Replace atau Add
    * Contoh dibaris 37 saya gunakan fungsi Replace
    * */
    private fun loadFragment(fragment: Fragment, bundle: Bundle? = null) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (bundle != null) {
            fragment.arguments = bundle
        }
        fragmentTransaction.replace(R.id.fragment, fragment)
        fragmentTransaction.commit()
    }

    companion object {
        const val KEY_TEXT = "KEY_TEXT"
    }
}